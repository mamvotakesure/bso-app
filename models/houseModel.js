const mongoose = require('mongoose');

const houseSchema = new mongoose.Schema({
  description: {
    type: String,
    required: [true, 'The field description should not be empty.'],
    trim: true,
  },
  imageCover: {
    type: String,
    required: [true, 'A house must have a cover image'],
  },
  rooms: {
    type: Number,
  },
  beds: {
    type: Number,
  },

  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
  },
});

const HouseModel = mongoose.model('House', houseSchema);
module.exports = HouseModel;
