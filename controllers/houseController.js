const House = require('../models/houseModel');
const createResponse = require('../utils/createResponse');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/AppError');

exports.createHouse = catchAsync(async (req, res, next) => {
  const newHouse = await House.create(req.body);

  createResponse(newHouse, 201, res);
});

exports.getAllHouses = catchAsync(async (req, res, next) => {
  const houses = await House.find();

  createResponse(houses, 200, res);
});

exports.getHouse = catchAsync(async (req, res, next) => {
  const house = await House.findById(req.params.id);

  if (!house)
    return next(new AppError('The house with this id does not exist', 404));

  createResponse(house, 200, res);
});

exports.updateHouse = catchAsync(async (req, res, next) => {
  const house = await House.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  if (!house)
    return next(new AppError('The house with this id does not exist', 404));

  createResponse(house, 200, res);
});

exports.deleteHouse = catchAsync(async (req, res, next) => {
  const house = await House.findByIdAndDelete(req.params.id);

  if (!house)
    return next(new AppError('The house with this id does not exist', 404));

    createHouse(null, 200, res)
});
