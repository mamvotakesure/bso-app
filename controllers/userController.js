const User = require('../models/userModel');
const AppError = require('../utils/AppError');
const catchAsync = require('../utils/catchAsync');
const createResponse = require('../utils/createResponse');

exports.getAllUser = catchAsync(async (req, res, next) => {
  const users = await User.find();

  createResponse(users, 201, res);
});

exports.getUser = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.params.id);

  if (!doc) return next(new AppError('No user found with this id', 404));

  createResponse(user, 201, res);
});

exports.updateUser = catchAsync(async (req, res, next) => {
  const user = await User.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  if (!user)
    return next(new AppError('The user with this id does not exist', 404));

  createResponse(user, 201, res);
});

exports.deleteUser = catchAsync(async (req, res, next) => {
  const doc = await User.findByIdAndDelete(req.params.id);

  if (!doc) return next(new AppError('No user found with this id', 404));

  createResponse(null, 201, res);
});
