const log = require('./utils/logger')
const app = require('./app');
const db = require('./utils/connect')

const PORT = process.env.PORT || 3000;

app.listen(PORT, async ()=>{
    log.info(`Server listening on ${PORT}`)
    await db.connect();
})

module.exports = app