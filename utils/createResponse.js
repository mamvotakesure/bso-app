const createResponse = (doc, statusCode, res) => {
  res.status(statusCode).json({
    status: 'success',
    data: {
      doc: doc,
    },
  });
};

module.exports = createResponse;