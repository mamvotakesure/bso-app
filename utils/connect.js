const mongoose = require("mongoose");
const logger = require('./logger')

exports.connect = async () => {
  const dbUri = process.env.DATABASE_URL.replace(
    "<PASSWORD>",
    process.env.DATABASE_PASSWORD
  );
  try {
    await mongoose.connect(dbUri)
    logger.info("Db Connected successifully")

  }catch(err) {
      logger.error('Error occured while connecting to the Database')
      process.exit(1)
  }

};

