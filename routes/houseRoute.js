const express = require('express');
const houseController = require('../controllers/houseController');
const authController = require('../controllers/authController');

const router = express.Router();

router.use(authController.protect);

router
  .route('/')
  .post(houseController.createHouse)
  .get(houseController.getAllHouses);

router
  .route('/:id')
  .get(houseController.getHouse)
  .patch(houseController.updateHouse)
  .delete(houseController.deleteHouse);

module.exports = router;
