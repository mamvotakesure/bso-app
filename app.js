const express = require("express");
const log = require("./utils/logger");
const dotenv = require("dotenv");
dotenv.config();

const userRouter = require('./routes/userRoute')
const houseRouter = require('./routes/houseRoute')

const app = express();
app.use(express.json());


app.use('/api/v1/users', userRouter);
app.use('/api/v1/houses', houseRouter);

module.exports = app;
